import {
  __addClass,
  __removeClass,
  __hasClass,
  __dispatchEvent,
} from '../../lib/utils';

import SVG from '../svg';
import SVGDragNDrop from './svg-dragndrop';

export default class SVGText {
  static createText(maxLength = 333, minLength = 2) {
    const $svg = document.querySelector('[data-svg-active="true"]');
    const svgID = $svg.id;
    const svgGroup = d3.select($svg).select('.svg-view');

    // const $canvas = $svg.querySelector('.svg-view');
    const $radialMenu = document.querySelector('[data-radial-menu]');
    const localPosition = {
      x: $radialMenu.getAttribute('data-local-x'),
      y: $radialMenu.getAttribute('data-local-y'),
    };
    const id = `${svgID}_svg-text-${localPosition.x}-${localPosition.y}`;
    const hash = `#${id}`;
    const orientation = {
      x: (svgGroup.attr('width') - localPosition.x) > 320 ? 'right' : 'left',
      y: (svgGroup.attr('height') - localPosition.y) > 60 ? 'bottom' : 'top',
    };
    // let firstWrite = true;

    // text wrapper
    const foreignObject = svgGroup.append('svg:foreignObject')
      .attr('class', `svg-text orientation-${orientation.x} orientation-${orientation.y}`)
      .attr('data-svg-text', '')
      .attr('x', localPosition.x)
      .attr('y', localPosition.y)
      .attr('data-svg-text-orientation', `${orientation.x}, ${orientation.y}`)
      .attr('data-svg-resolution', `${svgGroup.attr('width')}, ${svgGroup.attr('height')}`)
      .attr('width', 1)
      .attr('height', 1);

    // create visual point of interest for text input
    foreignObject.append('xhtml:div')
      .attr('class', 'svg-circle svg-circle-text');
    // create text input
    const textarea = foreignObject.append('xhtml:textarea')
      .attr('maxlength', maxLength)
      .attr('tabindex', '-1')
      .attr('id', id)
      .attr('class', 'svg-textarea')
      .attr('data-save', '')
      .attr('data-svg-textarea-target', svgID);

    // set focus to the new text input
    $svg.querySelector(hash).focus();

    // add events to element
    SVGText.textEventsManager($svg, textarea.node(), true, minLength);
  }

  static textEventsManager($svg, $node, isCreated, minLength = 2) {
    const $canvas = $svg.querySelector('.svg-view');
    const hash = `#${$node.id}`;
    let firstWrite = isCreated;
    let oldText = $node.value;

    // TODO
    // standalone function
    // same for all draggable svg elements
    const $element = $svg.querySelector(hash).parentElement;
    let elementPosition = [
      $element.getAttribute('x'),
      $element.getAttribute('y'),
    ];
    let diff = {};

    d3.select(hash)
      // drag & drop
      .call(d3.drag()
        .container($canvas)
        .subject($element)
        .on('start', () => {
          diff = {
            x: d3.event.x - elementPosition[0],
            y: d3.event.y - elementPosition[1],
          };

          SVGDragNDrop.start($element);
        })
        .on('drag', () => {
          SVGDragNDrop.dragged($element, 'text', diff, $node);
        })
        .on('end', () => {
          const oldXCoordinate = elementPosition[0];
          const oldYCoordinate = elementPosition[1];
          let taskType = 'move';

          SVGDragNDrop.end($element, 'text', diff, $node);

          elementPosition = [
            $element.getAttribute('x'),
            $element.getAttribute('y'),
          ];

          if (!firstWrite && (elementPosition[0] !== oldXCoordinate || elementPosition[1] !== oldYCoordinate)) {
            // if clone is changed, then dispatch create event
            if (SVG.removeCloneConnectionManager($element.childNodes[1])) taskType = 'create';

            // dispatch event that svg has changed
            __dispatchEvent(document, 'svg.change', {}, {
              svg: {
                id: $svg.id,
                task: taskType,
                node: $element.childNodes[1],
                nodeName: $element.childNodes[1].nodeName,
                contextMenu: false,
              },
            });
          }
        }))
      // keystroke
      // control height of the text input on every key stroke
      .on('keyup', () => {
        SVGText.updateTextareaHeight(d3.event.target);
      })
      // loss of focus
      .on('blur', () => {
        const $target = d3.event.target;
        const { value } = $target;
        let taskType;
        let wrongBool = false;

        if (value.length === 0) {
          SVG.removeItem(false, hash);
          return;
        }
        if (value.length < minLength) {
          __addClass($target, 'is-wrong');
          wrongBool = true;
        } else if ((value.length >= minLength) && __hasClass($target, 'is-wrong')) {
          __removeClass($target, 'is-wrong');
          wrongBool = false;
        }

        if (oldText !== value) {
          if (firstWrite) {
            taskType = 'create';
            firstWrite = false;
          } else {
            taskType = 'change';
          }
          oldText = value;
          // if clone is changed, then dispatch create event
          if (SVG.removeCloneConnectionManager($target)) taskType = 'create';

          // dispatch event that svg has changed
          __dispatchEvent(document, 'svg.change', {}, {
            svg: {
              id: $svg.id,
              task: taskType,
              node: $target,
              nodeName: $target.nodeName,
              isWrong: wrongBool,
              contextMenu: false,
            },
          });
        }

        // update oldText
        oldText = value;
      })
      // gain focus
      .on('focus', () => {
        d3.select($element).raise();
      });
  }

  static updateTextareaHeight($target) {
    $target.style.height = '1px';
    $target.style.height = `${22 + $target.scrollHeight}px`;
  }

  static comicBubblesEventsManager($svg, $node) {
    let firstWrite = true;
    let oldText = $node.value;

    d3.select($node).on('keyup', () => {
      SVGText.updateTextareaHeight(d3.event.target);
    })
      .on('blur', () => {
        const $target = d3.event.target;
        const { value } = $target;
        let taskType;

        if (oldText !== value) {
          if (firstWrite) {
            taskType = 'create';
            firstWrite = false;
          } else {
            taskType = 'change';
            if (value.length === 0) {
              firstWrite = true;
            }
          }
          oldText = value;
          // if clone is changed, then dispatch create event
          if (SVG.removeCloneConnectionManager($target)) taskType = 'create';

          // dispatch event that svg has changed
          __dispatchEvent(document, 'svg.change', {}, {
            svg: {
              id: $svg.id,
              task: taskType,
              node: $target,
              nodeName: $target.nodeName,
              isBubble: true,
              contextMenu: false,
            },
          });
        }

        // update oldText
        oldText = value;
      });
  }
}
