/**
 * Defined all supported html nodes in editor
 * Divided in nodes and marks
 * Nodes are static - https://prosemirror.net/docs/ref/#model.NodeSpec
 * Marks are dynamic - https://prosemirror.net/docs/ref/#model.MarkSpec
 * Example: https://github.com/ProseMirror/prosemirror-schema-basic/blob/master/src/schema-basic.js
 */
const schemaConfig = {
  nodes: {
    doc: { content: 'block+' },
    heading: {
      attrs: {
        level: { default: 1 },
        style: { default: false },
      },
      content: 'inline*',
      group: 'block',
      defining: true,
      parseDOM: [{
        tag: 'h1',
        getAttrs(dom) {
          return { level: 1, style: dom.getAttribute('style') };
        },
      },
      {
        tag: 'h2',
        getAttrs(dom) {
          return { level: 2, style: dom.getAttribute('style') };
        },
      },
      {
        tag: 'h3',
        getAttrs(dom) {
          return { level: 3, style: dom.getAttribute('style') };
        },
      },
      {
        tag: 'h4',
        getAttrs(dom) {
          return { level: 4, style: dom.getAttribute('style') };
        },
      },
      {
        tag: 'h5',
        getAttrs(dom) {
          return { level: 5, style: dom.getAttribute('style') };
        },
      },
      {
        tag: 'h6',
        getAttrs(dom) {
          return { level: 6, style: dom.getAttribute('style') };
        },
      }],
      toDOM(node) { const { style } = node.attrs; return [`h${node.attrs.level}`, { style }, 0]; },
    },
    paragraph: {
      content: 'inline*',
      group: 'block',
      defining: true,
      attrs: {
        style: { default: false },
      },
      parseDOM: [{
        tag: 'p',
        getAttrs(dom) {
          return { style: dom.getAttribute('style') };
        },
      }],
      toDOM(node) { const { style } = node.attrs; return ['p', { style }, 0]; },
    },
    list: {
      content: 'block+',
      group: 'block',
      attrs: {
        type: { default: false },
      },
      parseDOM: [{
        tag: 'ol',
        getAttrs(dom) {
          return { type: dom.getAttribute('type') };
        },
      }],
      toDOM(node) { const { type } = node.attrs; return ['ol', { type }, 0]; },
    },
    listItem: {
      content: 'inline*',
      group: 'block',
      parseDOM: [{ tag: 'li' }],
      toDOM() { return ['li', 0]; },
    },
    hard_break: {
      inline: true,
      group: 'inline',
      selectable: false,
      parseDOM: [{ tag: 'br' }],
      toDOM() { return ['br']; },
    },
    text: {
      inline: true,
      group: 'inline',
    },
    // premarked parts
    premarked: {
      content: 'inline',
      inline: true,
      group: 'inline',
      attrs: {
        id: {},
        textData: {},
        className: {},
      },
      parseDOM: [{
        tag: 'span.text-option',
        getAttrs: ($dom) => {
          const id = $dom.getAttribute('id');
          const textData = $dom.getAttribute('data-text-option');
          const className = $dom.getAttribute('class');
          return { id, textData, className };
        },
      }],
      toDOM: (node) => {
        const { id, textData, className } = node.attrs;
        return ['span', {
          'id': id,
          'class': className,
          'data-text-option': textData,
        }, 0];
      },
    },
    // marker remove button
    removeButton: {
      inline: true,
      group: 'inline',
      selectable: false,
      parseDOM: [{ tag: 'span.node-close' }],
      toDOM() {
        return ['span', { class: 'node-close' }, '×'];
      },
    },
  },
  marks: {
    italic: {
      parseDOM: [{ tag: 'i' }, { tag: 'em' }],
      toDOM() { return ['em', 0]; },
    },
    bold: {
      parseDOM: [{ tag: 'b' }],
      toDOM() { return ['b', 0]; },
    },
    code: {
      parseDOM: [{ tag: 'code' }],
      toDOM() { return ['code', 0]; },
    },
    small: {
      parseDOM: [{ tag: 'small' }],
      toDOM() { return ['small', 0]; },
    },
    marker: {
      attrs: {
        color: {},
        dataCommandName: {},
        dataTagId: {},
      },
      parseDOM: [{
        tag: 'span[data-tag-id]',
        getAttrs: ($dom) => {
          const color = $dom.getAttribute('data-color');
          const dataCommandName = $dom.getAttribute('data-command-name');
          const dataTagId = $dom.getAttribute('data-tag-id');
          return { color, dataCommandName, dataTagId };
        },
      }],
      toDOM: (node) => {
        const { color, dataCommandName, dataTagId } = node.attrs;
        return ['span', {
          'class': `node-selected node-text-${color}`,
          'data-command-name': dataCommandName,
          'data-tag-id': dataTagId,
          'data-color': color,
        }, 0];
      },
    },
    writtingInsertPart: {
      attrs: {
        id: {},
      },
      parseDOM: [{
        tag: 'span.user-inserted-text.user-inserted-text-inner',
        getAttrs: ($dom) => {
          const id = $dom.getAttribute('data-add-id');
          return { id };
        },
      }],
      toDOM: (node) => {
        const { id } = node.attrs;
        return ['span', {
          'class': 'user-inserted-text user-inserted-text-inner',
          'data-add-id': id,
        }, 0];
      },
    },
    writtingRemovePart: {
      attrs: {
        id: {},
      },
      parseDOM: [{
        tag: 'span.user-removed-text.user-removed-text-inner',
        getAttrs: ($dom) => {
          const id = $dom.getAttribute('data-remove-id');
          return { id };
        },
      }],
      toDOM: (node) => {
        const { id } = node.attrs;
        return ['span', {
          'class': 'user-removed-text user-removed-text-inner',
          'data-remove-id': id,
        }, 0];
      },
    },
  },
};

export default schemaConfig;
