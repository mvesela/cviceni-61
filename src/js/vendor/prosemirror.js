// https://prosemirror.net/docs/ref/#commands
// https://github.com/ProseMirror/prosemirror-commands
import { toggleMark } from 'prosemirror-commands';
// https://prosemirror.net/docs/ref/#history
// https://github.com/ProseMirror/prosemirror-history
import { history, undo, redo } from 'prosemirror-history';
// https://prosemirror.net/docs/ref/#keymap
// https://github.com/ProseMirror/prosemirror-keymap
import { keymap, keydownHandler } from 'prosemirror-keymap';
// https://prosemirror.net/docs/ref/#model
// https://github.com/ProseMirror/prosemirror-model
import { DOMParser, MarkType, Schema } from 'prosemirror-model';
// https://prosemirror.net/docs/ref/#state
// https://github.com/ProseMirror/prosemirror-state
import { EditorState, Plugin, PluginKey, TextSelection } from 'prosemirror-state';
// https://prosemirror.net/docs/ref/#transform
// https://github.com/ProseMirror/prosemirror-transform
import { RemoveMarkStep } from 'prosemirror-transform';
// https://prosemirror.net/docs/ref/#view
// https://github.com/ProseMirror/prosemirror-view
import { EditorView } from 'prosemirror-view';

export {
  toggleMark,
  history, undo, redo,
  keymap, keydownHandler,
  DOMParser, MarkType, Schema,
  EditorState, Plugin, PluginKey, TextSelection,
  RemoveMarkStep,
  EditorView,
};
