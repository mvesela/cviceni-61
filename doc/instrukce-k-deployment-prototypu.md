# Instrukce k deploymentu
- **Vývojový repozitář** (zůstává beze změny): https://bitbucket.org/historylab/cviceni/src/dev/
- **Deployovací repozitář**: https://github.com/historylab-cz/cviceni-test
- **Nová URL**: https://cviceni-test.historylab.now.sh

## Checklist pro vývoj
- Názvy větví ve vývojovém repu by měly mít formát `feature/nazev-featury` nebo `cviceni/nazev-cviceni` (např. `cviceni/napalm-girl`) . Formát se jménem (např. `maha/nejaky-experiment`) používejte jen pro nějaké experimenty:)
- Jedno cvičení === jedna větev.
- Jednodušší optimalizace klidně rovnou do větve `dev`
- V mé nepřítomnosti si zkuste pull requesty vzájemně zkontrolovat mez sebou;)

## Checklist před deployem
1. Před kopírováním do deploy repa se ujistěte, že ve vývojovém repu pracujete s nejaktuálnější verzí dané branche (pull). Platí i pro deploy repo. Mohli byste v deploy repu přepsat nějaký kód, který by přepsaný být neměl:)
2. Název branche ve vývojovém repu odpovídá názvu složky v deploy repu, kde lomítko `/` v názvu branche odděluje jednotlivé úrovně ve složkové struktuře.
3. Pokud daná složka již existuje, nebojte se přepsat její veškerý obsah. Git změny zaznamená.

## Deploy
1. Na svém počítači ve vývojovém repu spusťte příkaz `gulp --production`
2. Zkopírujte obsah složky `dist/cviceni/` do správné složky v deploy repu (viz checklist)
> Pro lepší přehlednost je vhodnější nahrávat jen právě vyvíjené cvičení. Tudíž pokud je branch např. `cviceni/nove-cviceni` není třeba ze složky `cviceni` kopírovat nic jiného než složku `assets` a `nove-cviceni`.
3. Dejte commit a push. Deployment se spustí automaticky. 
4. Výsledek je během několika vteřin k vidění na https://cviceni-test.historylab.now.sh + složková struktura

## Komunikace s autory na Trellu
- Jakmile je výsledek live, dejte to vědět do dané karty v Trellu. Nejlépe i s linkem na dané cvičení a případnými komentáři.
- V současné nástěnce Didaktika jsou všude odkazy na www.historylab/test/dev . Bude potřeba teď důsledně komunikovat změnu URL na https://cviceni-test.historylab.now.sh .
- Některé karty mají tento odkaz připnutý jako přílohu. Pokud si toho všimnete, tak ho prosím změňte na novou URL.

Pokud se s tímto způsobem deploymentu objeví nějaký zásadní problém, Karel má přístupy na FTP a bude se to řešit starou cestou. 
